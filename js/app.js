const game = new Game();

/** 
 * Listens for click on `#begin-game` and calls startGame() on game object
 */
//Ashley way
 document.getElementById('begin-game').addEventListener('click', function(){
  game.startGame();
  this.style.display = 'none';
  document.getElementById('play-area').style.opacity = '1';
  
 });

//My first way
// $('#begin-game').on('click', 'button', ()=>{
//   this.style.display = 'none';
//   document.getElementById('play-area').style.opacity = '1';
//   game.startGame();
// });

//Alternative way

// const event = function(){
//   this.style.display = 'none';
//   document.getElementById('play-area').style.opacity = '1';
//   game.startGame();
// }

// document.getElementById('begin-game').addEventListener('click', event);


document.addEventListener('keydown', function(event){
  game.handleKeydown(event);
});
